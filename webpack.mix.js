const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss'); 
require('dotenv').config();

mix.js('resources/js/app.js', 'assets/js')
    .copy('resources/css/fonts', 'assets/fonts')
    .copy('resources/images', 'assets/images')
    .postCss('resources/css/app.css', 'assets/css', [
        tailwindcss("tailwind/package/tailwind.config.js"),
    ])
    //uncomment this to add more tailwind themes
   /* 
   .postCss('resources/css/filament.css', 'public/css', [
        tailwindcss("tailwind/filament/tailwind.config.js"),
        require('autoprefixer'),
    ])
    */
    .version()
    .disableNotifications();
