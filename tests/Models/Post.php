<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use KDA\Tests\Database\Factories\PostFactory;


class Post extends Model 
{
   
    use HasFactory;

    protected $fillable = [
        'title'
    ];

   
    protected static function newFactory()
    {
        return PostFactory::new();
    }
}
