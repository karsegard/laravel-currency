const __extends = require('./extends');
const __screens = require('./screens');
const __fluid = require('./fluid')
const __fontFamilies = require ('./font-families');
const __colors = require ('./colors');
const __theme = require ('./theme');
const plugin = require('tailwindcss/plugin')

module.exports = {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
        "./vendor/kda/laravel-layouts/resources/views/**/*.blade.php",
        "./vendor/kda/livewire-modal/resources/views/*.blade.php",
        "./vendor/kda/blade-tiptap-renderer/resources/views/**/*.blade.php",
        "./vendor/kda/eloquent-medialibrary-item/resources/views/**/*.blade.php",
    ],
    darkMode: 'class',
    safelist: [
        'italic','font-bold','mce_inline_error',
        {
           pattern: /max-w-(sm|md|lg|xl|2xl|3xl|4xl|5xl|6xl|7xl)/,
           variants: ['sm', 'md', 'lg', 'xl', '2xl'],
        },
        'sm:bg-[image:var(--background-image-sm)]',
        'md:bg-[image:var(--background-image-md)]',
        'xl:bg-[image:var(--background-image-xl)]',

     ],
    textSizes: {
        ...__fluid.textSizes
    },

    leading: {
        ...__fluid.leading

    },

    theme: {
        ...__theme,

        screens: __screens,


        extend: {
            fontFamily: {
                ...__fontFamilies
            },
            colors: {
                ...__colors
            },
            ...__extends
        },
    },
    plugins: [
        require('@tailwindcss/typography'),
        plugin(function({ addVariant }) {
            addVariant('not-first', '&:not(:first-child)');
            addVariant('not-last', '&:not(:last-child)');
            addVariant('not-edging', '&:not(:first-child):not(:last-child)');
            addVariant('edging', ['&:first-child','&:last-child']);
            addVariant('ios-touch','@supports(-webkit-touch-callout : none)');
            addVariant('not-ios-touch','@supports not (-webkit-touch-callout : none)');
        }),
        plugin(function({addUtilities}){
            addUtilities({
                ".running": { animationPlayState: "running" },
                ".paused": { animationPlayState: "paused" },
            })
        }),
        require('tailwindcss-fluid')({
            textSizes: true,
            leading: true
        })
    ],
}
