module.exports = {
    'primary': ["Oroban-Elegans", "serif"],
    'primary-italic': ["Oroban-ElegansItalic", "serif"],
    'secondary': ["MonumentExtendedUltralight", "sans-serif"],
    'secondary-bold':["MonumentExtended-Bold", "sans-serif"],
   
}