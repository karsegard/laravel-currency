<?php
namespace KDA\Laravel\Currency;

use Closure;
use KDA\Laravel\Currency\Concerns\EvaluatesClosure;

//use Illuminate\Support\Facades\Blade;
class CurrencyService 
{
    use EvaluatesClosure;
    
    protected ?Closure  $get_currency_digits_callback = null;

    public function getCurrencyDigitsUsing(Closure $callback):static
    {
        $this->get_currency_digits_callback = $callback;
        return $this;
    }

    public function getCurrencyDigits($model,$attribute):?int
    {
        return $this->evaluate($this->get_currency_digits_callback,$this->getEvaluationParameters(['model'=>$model,'attribute'=>$attribute]));
    }
}