<?php

namespace KDA\Laravel\Currency\Facades;

use Illuminate\Support\Facades\Facade;

class CurrencyService extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
