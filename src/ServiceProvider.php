<?php
namespace KDA\Laravel\Currency;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Currency\Facades\CurrencyService as Facade;
use KDA\Laravel\Currency\CurrencyService as Library;
class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    protected $packageName ='laravel-currency';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
