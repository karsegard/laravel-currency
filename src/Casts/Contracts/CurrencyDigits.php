<?php

namespace KDA\Laravel\Currency\Casts\Contracts;


interface CurrencyDigits{

    public function getCurrencyDigits($attribute): ?int;
}