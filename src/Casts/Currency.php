<?php

namespace KDA\Laravel\Currency\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use KDA\Laravel\Currency\Casts\Contracts\CurrencyDigits;
use KDA\Laravel\Currency\Facades\CurrencyService;

class Currency implements CastsAttributes
{

    protected $default_digits = 2;

    protected ?int $digits = null;

    public function __construct(?int $digits = null)
    {
        if ($digits && $digits < 1) {
            throw new \InvalidArgumentException('Digits should be a number larger than zero.');
        }

        $this->digits = $digits;
    }

    public function getDigits($model, $attribute)
    {
        if (!blank($this->digits)) {
            return $this->digits;
        }

        //if not defined we check if model implements contract
        $class  = new \ReflectionClass($model);
        if ($class->implementsInterface(CurrencyDigits::class)) {
            $digits = $model->getCurrencyDigits($attribute);
            if (!blank($digits)) {
                return $digits;
            }
        }

        //if nothing yet, we ask the CurrencyService

        $digits = CurrencyService::getCurrencyDigits($model, $attribute);
        if (!blank($digits)) {
            return $digits;
        }
        
        return $this->default_digits;
    }

    public function get($model, string $key, $value, array $attributes): mixed
    {
        return $value !== null
            ? round($value / (10 ** $this->getDigits($model, $key)), $this->getDigits($model, $key))
            : null;
    }


    public function set($model, string $key, $value, array $attributes): mixed
    {
        return $value !== null
            ? (int) round($value * (10 ** $this->getDigits($model, $key)))
            : null;
    }
}
